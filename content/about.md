+++
date = "2016-06-30T22:10:20-03:00"
menu = "main"
title = "Sobre"
type = "about"
weight = -250
draft = true
+++

Olá, meu nome é Guto Carvalho, sou entusiasta de metodologias ágeis e me especializei em automação de infraestrutura.

Por mais de 16 anos eu trabalhei na operação de datacenters governamentais e privados,  toda essa experiência me possibilitou enxergar um caminho seguro para implantar metodologias ágeis e tecnologias de automação dentro de times de operação e infraestrutura.

O site Infra Ágil reune e organiza uma visão objetiva de como deve ser a transição de uma infra clássica para uma infra ágil.

O conteúdo tem base no trabalho que venho realizando nos últimos 5 anos em diversos projetos e clientes pelo Brasil, tendo como foco, tornar a infraestrutura destes clientes mais eficiente e autônoma.

Desde 2014 tenho evoluído esse conceito em parceria com Miguel Filho na Instruct, empresa pioneira em automação e gerência de configurações no Brasil.

Você podem saber mais sobre mim no site [gutocarvalho.net](http://gutocarvalho.net) e sobre o Miguel e a Instruct no site [instruct.com.br](http://instruct.com.br).
